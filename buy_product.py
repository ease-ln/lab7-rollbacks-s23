import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

update_alice_account = "UPDATE Player SET balance = '100' WHERE username = 'Alice'"
update_product_account = "UPDATE Shop SET in_stock = '10' WHERE product = 'marshmello'"

get_inventory_size = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
update_inventory = "INSERT INTO Inventory(username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + excluded.amount"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="postgres",
        host="localhost",
        port=5432
    )

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute(update_alice_account)
            cur.execute(update_product_account)

            cur.execute("START TRANSACTION")

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                cur.execute("ROLLBACK")
                raise Exception("Bad balance")
            
            cur.execute("END TRANSACTION")

    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("START TRANSACTION")

            try:

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    cur.execute("ROLLBACK")
                    raise Exception("Wrong product or out of stock")
                
                cur.execute(get_inventory_size, obj)
                # https://stackoverflow.com/questions/10195139/how-to-retrieve-sql-result-column-value-using-column-name-in-python


                result_set = cur.fetchall()
                cur_amount = 0 if (result_set[0] is None or result_set[0] is None) else result_set[0]
                if cur_amount[0] + amount > 100:
                    cur.execute("ROLLBACK")
                    raise Exception("Inventory is full")

                cur.execute(get_inventory_size, obj)
                cur.execute('COMMIT')

            except psycopg2.errors.CheckViolation as e:
                cur.execute("ROLLBACK")
                raise Exception("Product is out of stock")
            
            cur.execute("END TRANSACTION")

buy_product('Alice', 'marshmello', 2)